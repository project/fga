Field Group Anchor

To enable the module, you need to follow these steps:

* In the configuration form, define the node types that will display the anchors.
* An anchor will be displayed for those field groups that have the ID (CSS)
defined in their configuration and it must always be used with a hyphen (-)
instead of an underscore (_).
* Then you need to indicate the position of the pseudo field in the view mode;
currently, it only supports the use of the full view mode.
* Add a css class "fga-to-tab" to the field groups that wants to show as tabs.
