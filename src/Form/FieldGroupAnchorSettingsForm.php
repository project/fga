<?php

namespace Drupal\fga\Form;

use Drupal\Core\Entity\EntityTypeBundleInfoInterface;
use Drupal\Core\Form\ConfigFormBase;
use Drupal\Core\Form\FormStateInterface;
use Symfony\Component\DependencyInjection\ContainerInterface;

/**
 * Configure Field Group Anchor settings.
 */
class FieldGroupAnchorSettingsForm extends ConfigFormBase {

  /**
   * The entity type bundle info.
   *
   * @var \Drupal\Core\Entity\EntityTypeBundleInfoInterface
   */
  protected $entityTypeBundleInfo;

  /**
   * Constructs a new MiFormularioConfiguracion.
   *
   * @param \Drupal\Core\Entity\EntityTypeBundleInfoInterface $entity_type_bundle_info
   *   The entity type bundle info service.
   */
  public function __construct(EntityTypeBundleInfoInterface $entity_type_bundle_info) {
    $this->entityTypeBundleInfo = $entity_type_bundle_info;
  }

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container) {
    return new static(
      $container->get('entity_type.bundle.info')
    );
  }

  /**
   * {@inheritdoc}
   */
  public function getFormId() {
    return 'fga_field_group_anchor_settings';
  }

  /**
   * {@inheritdoc}
   */
  protected function getEditableConfigNames() {
    return ['fga.settings'];
  }

  /**
   * {@inheritdoc}
   */
  public function buildForm(array $form, FormStateInterface $form_state) {
    $bundle_info = $this->entityTypeBundleInfo->getBundleInfo('node');
    $bundle_list = [];
    foreach ($bundle_info as $key => $bundle) {
      $bundle_list[$key] = $bundle['label'];
    }
    $form['field_group_anchor_node_type'] = [
      '#type' => 'checkboxes',
      '#title' => $this->t('Node type'),
      '#description' => $this->t('Select the node types where you want to use the field group anchors'),
      '#options' => $bundle_list,
      '#default_value' => $this->config('fga.settings')->get('field_group_anchor_node_type'),
    ];
    return parent::buildForm($form, $form_state);
  }

  /**
   * {@inheritdoc}
   */
  public function submitForm(array &$form, FormStateInterface $form_state) {
    $this->config('fga.settings')
      ->set('field_group_anchor_node_type', $form_state->getValue('field_group_anchor_node_type'))
      ->save();
    parent::submitForm($form, $form_state);
  }

}
